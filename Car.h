#pragma once
#include "MatrixObject.h"


/**
 * @class Car
 * @brief Clase que representa al coche dentro de la matriz.
 * @details Clase hija de 'MatrixObject' con lo que tiene sus mismos atributos.
 */
class Car : public MatrixObject
{

public:

    /**
     * @brief Constructor por defecto de la clase 'Car'.
     * @details Constructor que llama al constructor por defecto de la clase 'MatrixObject'.
     */
    Car();

    /**
     * @brief Constructor que inicializa los valores de la clase 'Car'.
     * @details Constructor que llama al constructor de la clase 'MatrixObject' con los parámetros pasados.
     * @param x_ Número de fila en la matriz.
     * @param y_ Número de columna en la matriz.
     */
    Car(unsigned int x_, unsigned int y_);

    /**
     * @brief Constructor de copia de la clase 'Car'
     * @details Constructor que copia todo el contenido de un objeto del mismo tipo pasado por parámetro.
     * @param MO Referencia constante que apunta al objeto del cual queremos hacer la copia de los datos.
     */
    Car(const MatrixObject& MO);

    /**
     * @brief Destructor de la clase 'Car'.
     */
    virtual ~Car() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'Empty'
     * @return Retorna siempre false ya que el coche representa un contenido en la posición.
     */
    virtual bool is_empty() override;

    /**
     * @brief Método para mostrar por pantalla el contenido de la clase 'Car'.
     */
    virtual void show() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'End'
     * @return Retorna siempre false ya que es del tipo 'Car'.
     */
    virtual bool is_end() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'Block'
     * @return Retorna siempre false ya que el coche no es de tipo 'Block'.
     */
    virtual bool is_block() override;
};
