#include "Matrix.h"

#include "LOpenGL.h"
#include <unistd.h>

//ulimit -s 32767 para aumentar el tamaño del stack

const int SCREEN_WIDTH = 768;
const int SCREEN_HEIGHT = 420;
const int SCREEN_FPS = 60;
GLuint cubo, suelo;

Matrix* M;
Track Camino;
int cnt_camino = 0;
int cnt_muestra = 0;
int cam_anterior[3] = {0, 5, 5};

void handleKeys(unsigned char, int, int);
void runMainLoop(int);
void GenerateList(void);
bool initGL(void);
void update(void);
void render(void);

int main(int argc, char *argv[])
{
    int n_filas = -1;
    int n_columnas = -1;
/*
    int pos_x_car_ = 0;
    int pos_y_car_ = 0;

    int pos_x_end_ = 0;
    int pos_y_end_ = 0;
*/
    std::cout << "---- Introduzca un tamaño de matriz ----" << std::endl;
    std::cout << "\tNúmero de Filas: ";
    std::cin >> n_filas;

    std::cout << "\tNúmero de Columnas: ";
    std::cin >> n_columnas;

    M = new Matrix(n_filas, n_columnas);

    std::cout << std::endl;
    std::cout << std::endl;

    int op = -1;

    while (!(op == 2 || op == 1)) {

        std::cout << "--- Introduzca una opcion ---" << std::endl;
        std::cout << "\t1. Introducir Manualmente" << std::endl;
        std::cout << "\t2. Generar Aleatoriamente" << std::endl;
        std::cout << ">> ";
        std::cin >> op;

    }

    switch (op) {
        case 1:     // Introduciendo manualmente las posiciones de los obtáculos

            M->manual_fill_matrix();
            break;

        case 2:     // Introduciendo aleatoriamente las posiciones de los obtáculos indicando el porcentaje

            unsigned int percent;

            std::cout << "Introduzca un porcentaje de obstáculos: ";
            std::cin >> percent;

            M->random_fill_matrix(percent);
            break;

        default:
            break;
    }
    M->show();


    // Indicando posicion de origen del coche
    bool located = false;

    while (!located) {

        std::string pos_car;

        std::cout << "Introduzca la posición de origen del coche (x,y): ";
        std::cin >> pos_car;

        located = M->locate_car(pos_car);

    }


    // Indicando posicion de destino del coche
    located = false;

    while (!located) {

        std::string pos_car;

        std::cout << "Introduzca la posición de destino del coche (x,y): ";
        std::cin >> pos_car;

        located = M->locate_endPoint(pos_car);

        std::string camino;

        M->show();
        std::cout << std::endl;

    }

    Camino = M->A_estrella();

    if (Camino.getN_steps_() == 0) {

        std::cout << "No existe camino posible" << std::endl;

    } else {


        glutInit(&argc, argv); //Inicializamos freeglut
        glutInitContextVersion(2, 1); //Creamos un contexto opengl

        glutInitDisplayMode(GLUT_DOUBLE); //Creamos una ventana con doble buffer
        glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        glutCreateWindow("Practica_01_IA");

        if (!initGL()) {   //Comprobamos que podemos inicializar opengl
            std::cout << "-->Algo raro paso con las librerias gráficas!" << std::endl;
            return 1;
        } else {
            std::cout << "-->Las librerias gráficas se cargaron correctamente" << std::endl;
            GenerateList(); // dibujamos todos los objetos 3d que vamos a usar en la simulación
        }


        //  glutKeyboardFunc( handleKeys );   //hacemos que glut se encargue del input del usuario

        glutDisplayFunc(render);    //le indicamos a glut que queremos que use nuestra función como render

        glutTimerFunc(1000 / SCREEN_FPS, runMainLoop, 0);   //indicamos cual va a ser el loop principal de la app

        glutMainLoop();   //iniciamos glut
    }
}

void runMainLoop(int val){
  render();
  update();
  glutTimerFunc( 1000 / SCREEN_FPS, runMainLoop, val );  //hacemos otra llamada 1/60 seg para tener los 60 fps
}

void GenerateList(){
    suelo = glGenLists(1);   //se genera la lista cubo.

    glNewList(suelo, GL_COMPILE);

    glBegin(GL_LINES);

    GLfloat x = M->get_m();
    GLfloat z = M->get_n();

    glColor3f(0.282f, 0.820f, 0.800f);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(0.0f, 0.0f, -1 * z);

    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(1 * x, 0.0f, 0.0f);

    glVertex3f(x, 0.0f, 0.0f);
    glVertex3f(x, 0.0f, -1 * z);

    glVertex3f(x, 0.0f, -1 * z);
    glVertex3f(0.0f, 0.0f, -1 * z);

      //Lineas verticales
      for(GLfloat i = 0.0f; i < z; i += 1.0f){
          glVertex3f(0.0f, 0.0f,-1 * i);
          glVertex3f(x, 0.0f, -1 * i);
      }

      //Lineas horizontales
      for(GLfloat j = 0.0f; j > -x; j -= 1.0f){
          glVertex3f(-1 *j, 0.0f, 0.0f);
          glVertex3f(-1 *j, 0.0f, -1 * z);
      }

    glEnd(); // tenemos un grid redimensionable de cuadros de 1 x 1.

    glEndList();

    cubo = glGenLists(1);   //se genera la lista cubo.

    glNewList(cubo, GL_COMPILE);

    glBegin(GL_QUADS); // obstáculos del mapa y el coche

    std::vector<int> dummy = M->get_pos_car();

    for(int k = 0; k < M->get_m(); k++) {
        for (int j = 0; j < M->get_n(); j++) {
            if (M->get_celda(k, j)->is_empty() == false) {
                /*if( std::pair<unsigned int, unsigned int>(dummy[0],dummy[1]) == Camino.next_step(cnt_muestra) ) {
                    glColor3f(0, 0.5, 0.5);
                    cnt_muestra++;
                }*/
                if (M->get_celda(k, j)->is_block())
                    glColor3f(0.282f, 0.820f, 0.800f);
                glNormal3f(0.0f, 1.0f, 0.0f);
                glVertex3f(k, 0.0f, -1 * j);
                glVertex3f(k, 0.0f, -1 * j - 1);
                glVertex3f(k + 1, 0.0f, -1 * j - 1);
                glVertex3f(k + 1, 0.0f, -1 * j);
            }
            /*
            if (std::pair<unsigned int, unsigned int>(k, j) == Camino.next_step(cnt_muestra)) {
                glColor3f(0, 0.5, 0.5);
                cnt_muestra++;
            }
             */
            if (M->get_celda(k, j)->is_end()) {
                glColor3f(1, 1, 0);
                glNormal3f(0.0f, 1.0f, 0.0f);
                glVertex3f(k, 0.0f, -1 * j);
                glVertex3f(k, 0.0f, -1 * j - 1);
                glVertex3f(k + 1, 0.0f, -1 * j - 1);
                glVertex3f(k + 1, 0.0f, -1 * j);

            }
        }
    }

    cnt_muestra = 0;

    int i = dummy[0] +1;
    int j = dummy[1] +1;
    glColor3f(0.863f, 0.078f, 0.235f);

    //cara abajo
    glNormal3f(0, -1, 0);
    glVertex3f(i-1,   0, -1 * (j - 1)  );
    glVertex3f(i-1,   0, -1 * j        );
    glVertex3f(i  ,   0, -1 * j        );
    glVertex3f(i  ,   0, -1 * (j - 1)  );

    //cara arriba
    glNormal3f(0, 1, 0);
    glVertex3f(i-1, 1,  -1 * (j-1)     );
    glVertex3f(i-1, 1,  -1 * j         );
    glVertex3f(i,   1,  -1 * j         );
    glVertex3f(i,   1,  -1 * (j-1)     );

    //cara izquierda
    glNormal3f(-1, 0, 0);
    glVertex3f(i-1,   0,  -1 * (j-1) );
    glVertex3f(i-1,   1,  -1 * (j-1) );
    glVertex3f(i-1,   1,  -1 * j     );
    glVertex3f(i-1,   0,  -1 * j     );

    //cara frontal
    glNormal3f(0, 0, 1);
    glVertex3f(i,     0,  -1 * (j-1) );
    glVertex3f(i,     1,  -1 * (j-1) );
    glVertex3f(i-1,   1,  -1 * (j-1) );
    glVertex3f(i-1,   0,  -1 * (j-1) );

    //cara derecha
    glNormal3f(1, 0, 0);
    glVertex3f(i  ,   0,  -1 * j        );
    glVertex3f(i,     1,  -1 * j         );
    glVertex3f(i,     1,  -1 * (j-1)     );
    glVertex3f(i  ,   0,  -1 * (j - 1)  );

    //cara trasera
    glNormal3f(0, 0, -1);
    glVertex3f(i-1,     0,  -1 * (j-1) );
    glVertex3f(i-1,     1,  -1 * (j-1) );
    glVertex3f(i,   1,  -1 * (j-1) );
    glVertex3f(i,   0,  -1 * (j-1) );

    glEnd();

    glEndList();
}

bool initGL(){
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glMatrixMode( GL_PROJECTION );  //inicalizamos la matriz de proyección
    glLoadIdentity();
    gluPerspective(50.0f, (GLfloat)SCREEN_WIDTH / (GLfloat)SCREEN_HEIGHT, 0.5f, 50.0f);

    GLfloat x = M->get_m();
    GLfloat z = M->get_n();

    /*int cam[3];
    double angle = 60.0 * 3.14;
    std::vector<int> pos_car = M->get_pos_car();
    cam[0] = pos_car[0] + cos(angle) * 10;
    cam[1] = pos_car[1] + cos(angle) * 10;
    cam[2] = 0 + cos(angle) * 10;

    gluLookAt(cam[1], cam[0], cam[2], pos_car[1], pos_car[0], 0, 0, 1, 0);
     */

     //gluLookAt(7.5, 12, 8, 7.5, 0, -5, 0, 1, 0);
    //gluLookAt(x/2, x-3, z-5, x/2, 0, -5, 0, 1, 0);

    glMatrixMode( GL_MODELVIEW ); //inicializamos la matriz de vista de modelos
    glLoadIdentity();

    glClearColor( 0.f, 0.f, 0.f, 1.f ); //inicializamos el color del clear

    GLenum error = glGetError(); //comprobamos que no haya ningún error
    if( error != GL_NO_ERROR ){
        printf( "Error iniciando OpenGL! %s\n", gluErrorString( error ) );
        return false;
    }
    return true;
}

void update(){

    /* Actualizando posicion del coche */
    std::pair<unsigned int, unsigned int> pos_siguiente = Camino.next_step(cnt_camino);

    if ( cnt_camino < Camino.getN_steps_()) cnt_camino++;

    M->set_pos_car(pos_siguiente.first, pos_siguiente.second);
    /*  *************************************************************/

    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glMatrixMode( GL_PROJECTION );  //inicalizamos la matriz de proyección
    glLoadIdentity();
    gluPerspective(50.0f, (GLfloat)SCREEN_WIDTH / (GLfloat)SCREEN_HEIGHT, 0.5f, 50.0f);
    int cam[3];
    double angle = 60.0 * 3.14;
    double angle2 = (M_PI / 2) - angle;
    std::vector<int> pos_car = M->get_pos_car();
    cam[0] = pos_car[0] + cos(angle) * 5;
    cam[1] = pos_car[1] + cos(angle) * 5;
    cam[2] = sin(angle2) * 5;

    cam_anterior[0] = cam[0];
    cam_anterior[1] = cam[1];
    cam_anterior[2] = cam[2];

    gluLookAt(cam[0], cam[1], cam[2], pos_car[0], pos_car[1], 0, 0, 1, 0);
    usleep(500000);
    glMatrixMode( GL_MODELVIEW ); //inicializamos la matriz de vista de modelos
    glLoadIdentity();
    GenerateList();
}

void render(void){

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ); //limpiamos el buffer


    glMatrixMode( GL_MODELVIEW ); //reseteamos la matriz de modelos
    glLoadIdentity();

    glCallList(suelo);

    glMatrixMode( GL_MODELVIEW ); //reseteamos la matriz de modelos
    glLoadIdentity();

    glCallList(cubo);

   glutSwapBuffers(); //intercambia los buffers y actualiza la pantalla
}
