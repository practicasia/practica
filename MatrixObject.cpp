#include "MatrixObject.h"

MatrixObject::MatrixObject()
{}

MatrixObject::MatrixObject(unsigned int x_, unsigned int y_, char type_) :
        x_(x_),
        y_(y_),
        type_(type_)
{}

MatrixObject::MatrixObject(const MatrixObject& MO)
{
    this->x_ = MO.x_;
    this->y_ = MO.y_;

}

MatrixObject::~MatrixObject()
{

}


int MatrixObject::getX_() const
{
    return x_;
}

void MatrixObject::setX_(unsigned int x_)
{
    MatrixObject::x_ = x_;
}

int MatrixObject::getY_() const
{
    return y_;
}

void MatrixObject::setY_(unsigned int y_)
{
    MatrixObject::y_ = y_;
}

char MatrixObject::get_type() const{
  return type_;
}
