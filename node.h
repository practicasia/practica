#pragma once
#include <iostream>
#include <stdlib.h>


/**
 * @class node
 * @brief Clase que representa a un nodo de la matriz.
 * @details Contiene un la posición del nodo y su coste asociado.
 */
class node{

private:

    /** @var Componente 'x' de la posición del nodo */
    int x_;
    /** @var Componente 'y' de la posición del nodo */
    int y_;
    /** @var Coste más función heurística asociada al nodo */
    int prioridad_; //coste + heurística

public:

    /**
     * @brief Constructor de la clase 'node'.
     * @details Constructor que inicializa a los valores pasados por parámetros los atributos de la clase.
     * @param p Coste asociado al nodo.
     * @param xp Componente 'x' de la posición del nodo
     * @param yp Componente 'y' de la posición del nodo
     */
    node(int p, int xp, int yp);

    /**
     * @brief Método getter del atributo x_.
     * @return Componente 'x' de la posición del nodo.
     */
    int getx() const;

    /**
     * @brief Método getter del atributo y_.
     * @return Componente 'y' de la posición del nodo.
     */
    int gety() const;

    /**
     * @brief Método getter del atributo prioridad_.
     * @return Coste asociado al nodo.
     */
    int getPrioridad() const;

};

/**
     * @brief Sobrecarga del operador <.
     */
bool operator<(const node &a, const node &b);
