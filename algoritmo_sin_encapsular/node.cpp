#include "node.h"

node::node(int xp, int yp, int d, int p):
x_(xp),
y_(yp),
nivel_(d),
prioridad_(p){}

int node::getx() const{
  return x_;
}

int node::gety() const{
  return y_;
}

int node::getnv() const{
  return nivel_;
}

int node::getPrioridad() const{
  return prioridad_;
}

void node::updatePrioridad(int & xDest,int & yDest){
     prioridad_= nivel_ + estimar(xDest, yDest)*10;
}

void node::sigNiv(const int & i){
     nivel_ += 10;
}

int & node::estimar(int & xDest,int & yDest){
  static int xd, yd, d;
  xd = xDest - x_;
  yd = yDest - y_;

  d = abs(xd) + abs(yd);

  return(d);
}

bool operator<(const node &a, const node &b){
  return a.getPrioridad() > b.getPrioridad();
}
