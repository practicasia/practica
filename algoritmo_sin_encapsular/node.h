#pragma once
#include <iostream>
#include <stdlib.h>

class node{
    int x_;
    int y_;
    int nivel_; //coste
    int prioridad_; //coste + heurística

    public:
        node(int xp, int yp, int d, int p);

        int getx() const;
        int gety() const;
        int getnv() const;
        int getPrioridad() const;

        void updatePrioridad(int & xDest,int & yDest);
        void sigNiv(const int & i);
        int& estimar(int & xDest, int & yDest); //heurística
};


bool operator<(const node &a, const node &b);
