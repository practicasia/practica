#include "node.h"
#include <iostream>
#include <iomanip>
#include <queue>
#include <string>
#include <math.h>
#include <ctime>
#include <cstdlib>
#include <cstdio>

const int n=50;
const int m=50;

int map[n][m];

int nodos_cerrados[n][m];
int nodos_abiertos[n][m];

int map_direcciones[n][m];  //la idea seria sustituirlo por un vector

int dir=4;
int dx[4]={1, 0, -1, 0};
int dy[4]={0, 1, 0, -1};

std::string pathFind(int &xInit, int &yInit, int &xFin, int &yFin);

int main(){

    srand(time(NULL));

    for(int i=0; i < m; i++){
        for(int j=0; j<n; j++)
          map[i][j]=0;
    }

  /*  for(int x=0; x < 100;x++){
        map[rand() % 20][rand() % 20]=1;
    }*/

    int xA = 40, yA = 40, xB = 0, yB = 0;


    std::cout<<"Dimensiones (X,Y): "<<n<<","<<m<<std::endl;
    std::cout<<"Inicio: "<<xA<<","<<yA<<std::endl;
    std::cout<<"Final: "<<xB<<","<<yB<<std::endl;
    clock_t start = clock();
    std::string route=pathFind(xA, yA, xB, yB);
    if(route=="")
      std::cout<<"No hay camino posible"<<std::endl;
    clock_t end = clock();
    double tiempo_ejec = double(end - start);

    std::cout<<"Ruta:"<<std::endl;
    std::cout<<route<<std::endl<<std::endl;

    if(route.length()>0)
    {
        int j;
        char c;
        int x=xA;
        int y=yA;
        map[x][y]=2;
        for(int i=0;i<route.length();i++){
            c = route.at(i);
            j = atoi(&c);
            x = x+dx[j];
            y = y+dy[j];
            map[x][y]=3;
        }
        map[x][y]=4;

        for(int j=0; j<m; j++){
            for(int i=0; i<n; i++){
                if(map[i][j]==0)
                    std::cout<<" ";
                else if(map[i][j]==1)
                    std::cout<<"X";
                else if(map[i][j]==2)
                    std::cout<<"S";
                else if(map[i][j]==3)
                    std::cout<<"0";
                else if(map[i][j]==4)
                    std::cout<<"F";
            }
            std::cout<<std::endl;
        }
    }
      std::cout<<"Tiempo: "<<tiempo_ejec<<std::endl;
}


std::string pathFind(int & xInit, int & yInit, int & xFin, int & yFin){

    std::priority_queue<node> pq[2];
    int pqi = 0;
    node* n0;
    node* m0;
    int i, j, x, y, xdx, ydy;
    char c;

    for(y=0; y<m; y++){
        for(x=0; x<n; x++){
            nodos_cerrados[x][y] = 0;
            nodos_abiertos[x][y] = 0;
        }
    }

    n0=new node(xInit, yInit, 0, 0);

    n0->updatePrioridad(xFin, yFin);
    pq[pqi].push(*n0);
    nodos_abiertos[x][y]=n0->getPrioridad();

    delete n0;

    while(!pq[pqi].empty()){
        n0=new node( pq[pqi].top().getx(), pq[pqi].top().gety(),pq[pqi].top().getnv(), pq[pqi].top().getPrioridad());

        x=n0->getx();
        y=n0->gety();

        pq[pqi].pop();
        nodos_abiertos[x][y]=0;
        nodos_cerrados[x][y]=1;

        if(x==xFin && y==yFin){
            std::string path="";
            while(!(x==xInit && y==yInit)){
                j= map_direcciones[x][y];
                c= '0'+(j+dir/2) % dir;
                path= c+path;
                x+= dx[j];
                y+= dy[j];
            }

            delete n0;
            while(!pq[pqi].empty())
              pq[pqi].pop();
            return path;
        }

        for(i = 0;i < dir; i++){
            xdx= x + dx[i];
            ydy= y + dy[i];

            if(!(xdx<0 || xdx>n-1 || ydy<0 || ydy>m-1 || map[xdx][ydy]==1 ||
              nodos_cerrados[xdx][ydy]==1)){
                m0=new node( xdx, ydy, n0->getnv(), n0->getPrioridad());
                m0->sigNiv(i);
                m0->updatePrioridad(xFin, yFin);

                if(nodos_cerrados[xdx][ydy]==0){
                    nodos_abiertos[xdx][ydy]= m0->getPrioridad();
                    pq[pqi].push(*m0);
                    delete m0;
                    map_direcciones[xdx][ydy]= (i+dir/2)%dir;
                }
                else if(nodos_abiertos[xdx][ydy]>m0->getPrioridad()){
                    nodos_abiertos[xdx][ydy]= m0->getPrioridad();
                    map_direcciones[xdx][ydy]= (i+dir/2)%dir;

                    while(!(pq[pqi].top().getx() == xdx && pq[pqi].top().gety() == ydy)){
                        pq[1-pqi].push(pq[pqi].top());
                        pq[pqi].pop();
                    }
                    pq[pqi].pop();
                    if(pq[pqi].size()>pq[1-pqi].size()) pqi=1-pqi;
                      while(!pq[pqi].empty()){
                        pq[1-pqi].push(pq[pqi].top());
                        pq[pqi].pop();
                      }
                    pqi=1-pqi;
                    pq[pqi].push(*m0);
                    delete m0;
                }
                else
                  delete m0;
            }
         }
      }
    return "";
}
