#include "End.h"

End::End() :
        MatrixObject()
{}

End::End(unsigned int x_, unsigned int y_) :
        MatrixObject(x_, y_, 'E')
{}

End::End(const MatrixObject& MO) :
        MatrixObject(MO)
{}

End::~End()
{}

bool End::is_empty()
{
    return true;
}

void End::show()
{
    std::cout << "#";
}

bool End::is_end()
{
    return true;
}

bool End::is_block()
{
    return false;
}
