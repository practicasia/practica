#include "MatrixObject.h"

/**
 * @class End
 * @brief Clase que representa el punto de destino dentro de la matriz.
 * @details Clase hija de 'MatrixObject' con lo que tiene sus mismos atributos.
 */
class End : public MatrixObject
{

public:

    /**
     * @brief Constructor por defecto de la clase 'End'.
     * @details Constructor que llama al constructor por defecto de la clase 'MatrixObject'.
     */
    End();

    /**
     * @brief Constructor que inicializa los valores de la clase 'End'.
     * @details Constructor que llama al constructor de la clase 'MatrixObject' con los parámetros pasados.
     * @param x_ Número de fila en la matriz.
     * @param y_ Número de columna en la matriz.
     */
    End(unsigned int x_, unsigned int y_);

    /**
     * @brief Constructor de copia de la clase 'End'
     * @details Constructor que copia todo el contenido de un objeto del mismo tipo pasado por parámetro.
     * @param MO Referencia constante que apunta al objeto del cual queremos hacer la copia de los datos.
     */
    End(const MatrixObject& MO);

    /**
     * @brief Destructor de la clase 'End'.
     */
    virtual ~End() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'Empty'
     * @return Retorna siempre true ya que la posición de destino no contiene obstáculos y es transitable.
     */
    virtual bool is_empty() override;

    /**
     * @brief Método para mostrar por pantalla el contenido de la clase 'End'.
     */
    virtual void show() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'End'
     * @return Retorna siempre true ya que es del tipo 'End'(punto de destino del coche).
     */
    virtual bool is_end() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'Block'
     * @return Retorna siempre false ya que la posición de destino no representa a un obstáculo.
     */
    virtual bool is_block() override;

};
