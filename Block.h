#pragma once
#include "MatrixObject.h"


/**
 * @class Block
 * @brief Clase que representa al coche dentro de la matriz.
 * @details Clase hija de 'MatrixObject' con lo que tiene sus mismos atributos.
 */
class Block : public MatrixObject
{

public:

    /**
     * @brief Constructor por defecto de la clase 'Block'.
     * @details Constructor que llama al constructor por defecto de la clase 'MatrixObject'.
     */
    Block();

    /**
     * @brief Constructor que inicializa los valores de la clase 'Block'.
     * @details Constructor que llama al constructor de la clase 'MatrixObject' con los parámetros pasados.
     * @param x_ Número de fila en la matriz.
     * @param y_ Número de columna en la matriz.
     */
    Block(unsigned int x_, unsigned int y_);

    /**
     * @brief Constructor de copia de la clase 'Block'
     * @details Constructor que copia todo el contenido de un objeto del mismo tipo pasado por parámetro.
     * @param MO Referencia constante que apunta al objeto del cual queremos hacer la copia de los datos.
     */
    Block(const MatrixObject& MO);

    /**
     * @brief Destructor de la clase 'Block'.
     */
    virtual ~Block() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'Empty'
     * @return Retorna siempre false ya que representa un obstaculo en la posición.
     */
    virtual bool is_empty() override;

    /**
     * @brief Método para mostrar por pantalla el contenido de la clase 'Block'.
     */
    virtual void show() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'End'
     * @return Retorna siempre false ya que es del tipo 'Block'.
     */
    virtual bool is_end() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'Block'
     * @return Retorna siempre true ya que es de tipo 'Block'.
     */
    virtual bool is_block() override;

};
