#include "Node.h"

Node::Node():

        MatrixObject()

{}

bool Node::is_empty()
{
    return false;
}

bool Node::is_end()
{
    return false;
}

void Node::show()
{

}

Node::Node(unsigned int x_, unsigned int y_) :
        MatrixObject(x_, y_)
{}

Node::Node(unsigned int x_, unsigned int y_, double cost_, double aprox_) :
        MatrixObject(x_, y_, cost_, aprox_)
{}

Node::Node(const MatrixObject& MO) :
        MatrixObject(MO)
{}

