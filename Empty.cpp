#include "Empty.h"

Empty::Empty() :
        MatrixObject()
{}

Empty::Empty(unsigned int x_, unsigned int y_) :
        MatrixObject(x_, y_, '%')
{
}

Empty::Empty(const MatrixObject& MO) :
        MatrixObject(MO)
{}

Empty::~Empty()
{

}

bool Empty::is_empty()
{
    return true;
}

void Empty::show()
{
      std::cout << ' ';
}

bool Empty::is_end()
{
    return false;
}

bool Empty::is_block()
{
    return false;
}

bool operator<(const Empty &a, const Empty &b);
