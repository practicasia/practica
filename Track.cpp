#include "Track.h"

Track::Track():
        n_steps_(0)
{}

Track::Track(const Track& T)
{
    this->n_steps_ = T.n_steps_;
    this->cost_ = T.cost_;
    this->pos_actual_ = T.pos_actual_;
    this->track_ = T.track_;
}

Track::~Track()
{}

std::pair<unsigned int, unsigned int> Track::getPos_actual_() const
{
    return pos_actual_;
}

float Track::getCost_() const
{
    return cost_;
}

void Track::setCost_(float cost_)
{
    Track::cost_ = cost_;
}

int Track::getN_steps_() const
{
    return n_steps_;
}

void Track::append(std::pair<unsigned int, unsigned int> pos)
{
    n_steps_++;
    track_.push_back(pos);
}

void Track::front_append(std::pair<unsigned int, unsigned int> pos)
{
    n_steps_++;
    track_.insert(track_.begin(), pos);
}

std::pair<unsigned int, unsigned int> Track::get_last() const
{
    return track_.back();
}

bool Track::operator<(const Track& rhs) const
{
    return cost_ < rhs.cost_;
}

bool Track::operator>(const Track& rhs) const
{
    return rhs < *this;
}

bool Track::operator<=(const Track& rhs) const
{
    return !(rhs < *this);
}

bool Track::operator>=(const Track& rhs) const
{
    return !(*this < rhs);
}

bool Track::equivalent(const Track& rhs) const
{
    return this->get_last() == rhs.get_last();
}

void Track::start_track()
{
    pos_actual_ = track_.front();
}

std::pair<unsigned int, unsigned int> Track::next_step(int i)
{
    if ( i == 0 ) {
        this->start_track();
        return pos_actual_;
    }
    else {
        pos_actual_ = track_[i];
        return pos_actual_;
    }
}
