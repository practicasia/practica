#include "Block.h"

Block::Block() :
        MatrixObject()
{}

Block::Block(unsigned int x_, unsigned int y_) :
        MatrixObject(x_, y_, 'X')
{}

Block::Block(const MatrixObject& MO) :
        MatrixObject(MO)
{}

Block::~Block()
{}

bool Block::is_empty()
{
    return false;
}

void Block::show()
{
    std::cout << "+";
}

bool Block::is_end()
{
    return false;
}

bool Block::is_block()
{
    return true;
}
