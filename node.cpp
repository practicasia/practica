#include "node.h"

node::node(int p, int xp, int yp):
x_(xp),
y_(yp),
prioridad_(p){}

int node::getx() const{
  return x_;
}

int node::gety() const{
  return y_;
}

int node::getPrioridad() const{
  return prioridad_;
}

bool operator<(const node &a, const node &b){
  int cont_a;
  int cont_b;

  cont_a = (a.getPrioridad() *4) + (a.getx() * 2) + (a.gety());
  cont_b = (b.getPrioridad() *4) + (b.getx() * 2) + (b.gety());

  return cont_a < cont_b;
}
