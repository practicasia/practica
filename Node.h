#pragma once
#include "MatrixObject.h"

class Node : public MatrixObject
{

public:

    Node();
    Node(unsigned int x_, unsigned int y_);
    Node(unsigned int x_, unsigned int y_, double cost_, double aprox_);
    Node(const MatrixObject& MO);

    bool is_empty() override;

    bool is_end() override;

    void show() override;
};



