#include "Matrix.h"


int main(int argc, char *argv[])
{
    time_t initTime = time(NULL);

    std::fstream InputFile("./datos/input.txt", std::fstream::in);
    std::fstream OutputFile;
    OutputFile.open("./datos/data.json", std::fstream::out);
    OutputFile << "[" << std::endl;
    OutputFile.close();

    int iteraciones = 0;

    InputFile >> iteraciones;

    for ( int i = 0; i < iteraciones; ++i ) {

        int row = 0, col = 0, percent = 0;
        InputFile >> row >> col >> percent;

        std::cout << std::endl;
        std::cout << "Matriz: " << row << " x " << col << std::endl;
        std::cout << "\tObstaculos: " << percent << "%" << std::endl;

        Matrix* M;
        M = new Matrix(row, col);
        M->random_fill_matrix(percent);
        M->locate_car("0,0");
        M->locate_endPoint(std::to_string(row - 1) + "," + std::to_string(col - 1));
        M->A_estrella();

        if ( i != iteraciones-1 ) {

            OutputFile.open("./datos/data.json", std::fstream::out | std::fstream::app);
            OutputFile << "," << std::endl;
            OutputFile.close();
        }
        else {
            OutputFile.open("./datos/data.json", std::fstream::out | std::fstream::app);
            OutputFile << std::endl;
            OutputFile.close();
        }
    }

    OutputFile.open("./datos/data.json", std::fstream::out | std::fstream::app);
    OutputFile << "]" << std::endl;

    time_t endTime = time(NULL);

    double duration = difftime(endTime, initTime);

    std::cout << std::endl << "Duracion total: " << duration << " seg." << std::endl;

}