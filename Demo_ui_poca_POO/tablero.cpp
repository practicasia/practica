#include "tablero.h"

tablero::tablero(void):
n_(),
m_(),
densidad_(),
celdas_(){}

tablero::tablero(int n, int m, float r, std::vector<int> car):
n_(n),
m_(m),
densidad_(r),
celdas_(){
  std::cout << "-->Información de la tabla" << std::endl;
  int num = n_ * m_; // numero de casillas del tablero
  int num_u = num - (num * densidad_); // numero de casillas disponibles
  int index = 0;
  celda* dummy;

  std::cout << std::setw(2) << " " << "-Numero de casillas totales: " << num << std::endl;
  std::cout << std::setw(2) << " " << "-Numero de casillas disponibles: " << num_u << std::endl << std::endl;

  srand(time(NULL));

  int i = 0;
  while(i < (num-num_u)){
    int pos = rand() % num;
    if((pos != car[0]) && (pos != car[1])){
      obstaculos_.push_back(pos);
      i++;
    }
  }

  ord_pos();

  for(int i = 0; i < num; i++){
    if(i == obstaculos_[index]){
      index++;
      while(obstaculos_[index] == obstaculos_[index -1])
        index++;
      dummy = new celda('o', i);
    }
    else{
      dummy = new celda('r', i);
    }
    celdas_.push_back(*dummy);
  }
}

tablero::~tablero(void){
  n_ = 0;
  m_ = 0;
  densidad_ = 0;
}

void tablero::insert_obs(int x, int y){
  obstaculos_.push_back(get_pos(x,y));
  ord_pos();
}

celda& tablero::get_celda(int i, int j){
  return celdas_[get_pos(i,j)];
}

std::vector<int>& tablero::get_vec(void){
  return obstaculos_;
}

int& tablero::get_n(void){
  return n_;
}

int& tablero::get_m(void){
  return m_;
}

void tablero::print(void){
  int num = n_ * m_;
  for(int i = 0; i < num; i++){
    if((i % m_ == 0) && (i != 0)){
      std::cout << std::endl << celdas_[i].get_type() << " ";
    }
    else{
      std::cout << celdas_[i].get_type() << " ";
    }
  }
  std::cout << std::endl << std::endl;
}

void tablero::clean(void){
  std::cout << "Limpiando el entorno" << std::endl;
  for(int i = 0; i < obstaculos_.size(); i++){
    celdas_[obstaculos_[i]].get_type() = 'r';
  }
  obstaculos_.clear();
}

std::vector<int> tablero::pathfinder(int x1, int y1, int x2, int y2){
    std::vector<celda*> camino;

    celda inicio = get_celda(x1,y1);
    celda fin = get_celda(x2,y2);
    celda current;
    celda next;

    std::list<celda> openlist;
    std::list<celda> closedlist;
    std::list<celda>::iterator i1;
    std::list<celda>::iterator i2;

    unsigned n = 0;   //contador de iteraciones;

    openlist.push_back(inicio);
    inicio.get_opened() = true;

    while(n == 0 || (current.get_pos() != fin.get_pos() && n < 60)){
      for(i1 = openlist.begin(); i1 != openlist.end(); ++i1){
        /*if(i == openlist.begin() || (*i).getscore() <= current.getscore()){
          current = (*i);
          i2 = i1;
        }*/
      }

      if(current.get_pos() == fin.get_pos()){
        break;
      }

      openlist.erase(i2);
      current.get_opened() = false;

      closedlist.push_back(current);
      current.get_closed() = true;

      for(int x = -1; x < 2; x++){
        for(int y = -1; y < 2; y++){
          if(x == 0 && y == 0){
            continue;
          }
          next = get_celda(current.get_x() +x, current.get_y() +y);
          if(next.get_closed() == true || next.get_caminable() == false){
            continue;
          }
          if(next.get_opened()){
            if(next)
          }
          else{

          }
        }
      }
      n++;
    }
    while(current != inicio){
      path.push_back(current.get_pos());
      current

    }
    return path;
}

int tablero::get_pos(int i, int j){
  return i * (m_) + j;
}

void tablero::ord_pos(void){
  int aux;

  for(int i = 0; i < obstaculos_.size(); i++){
      for(int x = i; x < obstaculos_.size(); x++){
        if(obstaculos_[i] > obstaculos_[x]){
          aux = obstaculos_[i];
          obstaculos_[i] = obstaculos_[x];
          obstaculos_[x] = aux;
      }
    }
  }
}
