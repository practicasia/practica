#include "LOpenGL.h"
#include "celda.h"

class tablero{
	private:
		int n_;
		int m_;
		float densidad_;
		std::vector<celda> celdas_;
		std::vector<int> obstaculos_;
	public:
		tablero(void);
		tablero(int,int,float,std::vector<int>);
		~tablero(void);
		void insert_obs(int,int);
		int& get_n(void);
		int& get_m(void);
		celda& get_celda(int,int);
		std::vector<int>& get_vec(void);
		void print(void);
		void clean(void);
		std::vector<int> pathfinder(int x1, int y1, int x2, int y2);
	private:
		int get_pos(int,int);
		void ord_pos(void);
};
