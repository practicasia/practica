#include "celda.h"
/*Las celdas pueden ser de varios tipos:
    - tipo r --> no presentan ningún obstáculo
    - tipo o --> presentan obstáculos para el vehículo
    - tipo n --> se comportan como r
*/
celda::celda(char t, int pos):
type_(t),
pos_(pos),
closed_(false),
opened_(false),
caminable_(true){
  if(type_ != 'r')
    caminable_ = false;
}

celda::celda(void){
type_ = 'n';
}

celda::~celda(void){
}

char& celda::get_type(void){
  return type_;
}

int& celda::get_pos(void){
  return pos_;
}

int& celda::get_x(void){
  return pos_;
}

int& celda::get_y(void){
  return pos_;
}

bool& celda::get_opened(void){
  return opened_;
}

bool& celda::get_closed(void){
  return closed_;
}

bool& celda::get_caminable(void){
  return caminable_;
}
