#include "LOpenGL.h"

class celda{
	private:
		char type_;
		int  pos_;
		bool opened_;
		bool closed_;
		bool caminable_;
	public:
		celda(char, int);
		celda(void);
		~celda(void);
		char& get_type(void);
		int& get_pos(void);
		int& get_x(void);
		int& get_y(void);
		bool& get_opened(void);
		bool& get_closed(void);
		bool& get_caminable(void);
};
