#pragma once
#include <iostream>


/**
 * @class MatrixObject
 * @brief Clase abstracta que representa a cada una de las posiciones de la matriz.
 * @details Clase padre de las clases 'Empty', 'Car', 'Block' y 'End'.
 */
class MatrixObject
{

private:

    /** @var Posición en las filas de la matriz. */
    int x_;
    /** @var Posición en las columnas de la matriz. */
    int y_;
    /** @var Tipo de dato que contiene: Empty, Car, Block, End... */
    char type_;

public:

    /**
     * @brief Constructor por defecto de la clase 'MatrixObject'.
     * @details Constructor que deja vacía la clase 'MatrixObject' sin inicializar ningún valor.
     */
    MatrixObject();

    /**
     * @brief Constructor para inicializar los valores de 'MatrixObject'.
     * @details Constructor que inicializa el objeto a la posición pasada por parámetro y al tipo.
     * @param x_ Número de fila en la matriz.
     * @param y_ Número de columna en la matriz.
     * @param type_ Tipo de dato que contiene la posición especificada.
     */
    MatrixObject(unsigned int x_, unsigned int y_, char type_);

    /**
     * @brief Constructor de copia de la clase 'MatrixObject'
     * @details Constructor que copia todo el contenido de un objeto del mismo tipo pasado por parámetro.
     * @param MO Referencia constante que apunta al objeto del cual queremos hacer la copia de los datos.
     */
    MatrixObject(const MatrixObject& MO);

    /**
     * @brief Destructor de la clase 'MatrixObject'.
     */
    virtual ~MatrixObject();

    /**
     * @brief Método getter para obtener la variable 'x_'.
     * @return Componente x de la posición del objeto dentro de la matriz(int).
     */
    int getX_() const;

    /**
     * @brief Método setter para modificat la variable 'x_'.
     * @param x_ Nuevo valor de la variable 'x_'
     */
    void setX_(unsigned int x_);

    /**
     * @brief Método getter para obtener la variable 'y_'.
     * @return Componente y de la posición del objeto dentro de la matriz(int).
     */
    int getY_() const;

    /**
     * @brief Método setter para modificat la variable 'y_'.
     * @param x_ Nuevo valor de la variable 'y_'
     */
    void setY_(unsigned int y_);

    /**
     * @brief Método getter para obtener el tipo de objeto.
     * @return Caracter que representa a alguna de las clases hijas de 'MatrixObject'.
     */
    char get_type() const;

    /**
    * @brief Método virtual puro para conocer si una posición es de tipo 'Empty'.
    */
    virtual bool is_empty()=0;

    /**
    * @brief Método virtual puro para conocer si una posición es de tipo 'End'.
    */
    virtual bool is_end()=0;

    /**
    * @brief Método virtual puro para conocer si una posición es de tipo 'Block'.
    */
    virtual bool is_block()=0;

    /**
    * @brief Método virtual puro para mostrar por pantalla el objeto.
    */
    virtual void show()=0;

};
