#pragma once
#include "MatrixObject.h"
#include "Car.h"
#include "Block.h"
#include "Empty.h"
#include "End.h"
#include "node.h"
#include "Track.h"

// Librerias para generacion aleatoria
#include <stdlib.h>
#include <time.h>
// Libreria para comprobar si una cadena es numerica
#include <cctype>

#include <iostream>
#include <vector>
#include <string>
// Libreria para ordenar el vector
#include <algorithm>
#include <cmath>
#include <ctime>
#include <fstream>

struct data_t{
    double time;
    int percent;
    int steps;
    int row;
    int col;
    void exportData(std::string path)
    {
        std::fstream File(path, std::fstream::out | std::fstream::app);

        File << "\t{" << std::endl;
        File << "\t\t\"row\": " << row <<"," << std::endl;
        File << "\t\t\"col\": " << col <<"," << std::endl;
        File << "\t\t\"percent\": " << percent <<"," << std::endl;
        File << "\t\t\"steps\": " << steps <<"," << std::endl;
        File << "\t\t\"time\": " << time << std::endl;
        File << "\t}";
    }
};


/**
 * @class Matrix
 * @brief Clase que representa a una matriz para representar gráficamente el entorno.
 * @details Contiene "MatrixObject" que representa cada una de las celdas de la matriz.
 */
class Matrix
{

private:

    /** @var Número de filas de la matriz. */
    unsigned int m_;
    /** @var Número de columnas de la matriz. */
    unsigned int n_;
    /** @var std::vector de std::vector que representa la matriz.*/
    std::vector< std::vector <MatrixObject*> > V_;
    /** @var Componente 'x' de la posición inicial del coche. */
    int pos_x_coche_;
    /** @var Componente 'y' de la posición inicial del coche. */
    int pos_y_coche_;

    /** @var Componente 'x' de la posición de destino. */
    int pos_x_end_;
    /** @var Componente 'y' de la posición de destino. */
    int pos_y_end_;

    int percent;

public:

    /**
     * @brief Constructor por defecto de la clase 'Matrix'.
     * @details Constructor que deja vacía la clase 'Matrix' sin inicializar ningún valor.
     */
    Matrix();

    /**
     * @brief Constructor para inicializar la matriz a un tamaño.
     * @details Constructor que inicializa la matriz al tamaño pasado por parámetro. Construye la matriz con todas las posiciones inicializadas a tipo "Empty(Vacío)".
     * @param m_ Número de filas de la matriz.
     * @param n_ Número de columnas de la matriz.
     */
    Matrix(unsigned int m_, unsigned int n_);

    /**
     * @brief Destructor de la clase 'Matrix'.
     */
    ~Matrix();

    /**
     * @brief Método getter para obtener el número de filas.
     * @return Número de filas (unsigned int).
     */
    unsigned int get_m();
    /**
     * @brief Método getter para obtener el número de columnas.
     * @return Número de columnas (unsigned int).
     */
    unsigned int get_n();

    /**
     * @brief Método getter para obtener el contenido de una posición.
     * @details Método al que se le pasa por parámetros una posición y se obtiene su contenido.
     * @param i Número de fila al cual queremos acceder.
     * @param j Número de columna al cual queremos acceder.
     * @return Puntero a objeto de la clase abstracta 'MatrixObject'.
     */
    MatrixObject* get_celda(int i, int j);

    /**
     * @brief Método getter para obtener la posición del coche.
     * @details Método para obtener la posición actual del coche.
     * @return std::vector que contiene las dos componentes de la posición del coche.
     */
    std::vector<int> get_pos_car();

    /**
     * @brief Método setter para modificar la posición del coche.
     * @details Método al que se le pasa por parámetros las dos componentes de una posición y se modifica las posición del coche.
     * @param x Nueva componente 'x' de la posición del coche.
     * @param i Nueva componente 'y' de la posición del coche.
     */
    void set_pos_car(int x,int i);

    /**
     * @brief Método que rellena la matriz.
     * @details Método que rellena la matriz según el porcentaje pasado por parámetro.
     * @param percent Porcentaje de obstáculos que hay que introducir en la matriz.
     */
    void random_fill_matrix(unsigned int percent);

    /**
     * @brief Método que rellena la matriz.
     * @details Método que rellena la matriz especificando cada una de las posiciones en las que el usuario quiere introducir un obstáculo.
     */
    void manual_fill_matrix();

    /**
     * @brief Método que introduce al coche en la matriz.
     * @details Método que introduce al coche en la posición especificada por parámetro. El método evalúa la cadena y debe coincidir con el formato establecido.
     * @param pos_car Cadena de caracteres que debe ser del formato 'x,y' donde 'x' es la componente x de la posición e 'y' es la componente y de la posición.
     * @return En caso de que la cadena siga el formato y la posición pasada por parámetro sea vacía, se introducirá el coche en la matriz y se retorna true, en cualquier otro caso se retorna false.
     */
    bool locate_car(std::string pos_car);

    /**
     * @brief Método que introduce el punto de destino del coche en la matriz.
     * @details Método que introduce el punto de destino del coche en la posición especificada por parámetro. El método evalúa la cadena y debe coincidir con el formato establecido.
     * @param pos_car Cadena de caracteres que debe ser del formato 'x,y' donde 'x' es la componente x de la posición e 'y' es la componente y de la posición.
     * @return En caso de que la cadena siga el formato y la posición pasada por parámetro sea vacía, se introducirá el punto de destino del coche en la matriz y se retorna true, en cualquier otro caso se retorna false.
     */
    bool locate_endPoint(std::string pos_end);      // Posicionamiento del destino del coche

    /**
     * @brief Método para mostrar por pantalla la matriz.
     */
    void show();            // Mostrar por pantalla la matriz

    /**
     * @brief Método calcula un camino minimo entre el coche y el destino.
     * @details Método que halla el camino mínimo, en caso de existir, entre el punto inicial del coche y el de destino mediante el algoritmo de búsqueda A*.
     * @return Tanto como si existe como si no existe, se retorna un "Camino" representado por la clase 'Track'. En caso de estar vacío, no existe camino entre el punto inicial y el destino.
     */
    Track A_estrella();

};
