#pragma once
#include <vector>


/**
 * @class Track
 * @brief Clase que representa un camino en la matriz. Conjunto ordenado de posiciones.
 * @details Contiene "std::pair<unsigned int, unsigned int>" que representa cada una de las posiciones del camino.
 */
class Track
{
private:

    /** @var Posición actual dentro del camino. .first -> Componente x. .second -> Componente y. */
    std::pair<unsigned int, unsigned int> pos_actual_;
    /** @var std::vector de posiciones ordenadas según se han introducido */
    std::vector< std::pair<unsigned int, unsigned int> > track_;
    /** @var Coste total del camino. */
    float cost_;
    /** @var Número de pasos a dar para recorrer todo el camino. */
    int n_steps_;

public:

    /**
     * @brief Constructor por defecto de la clase 'Track'.
     * @details Constructor que deja vacía la clase 'Track' sin inicializar ningún valor.
     */
    Track();

    /**
     * @brief Constructor de copia de la clase 'Track'
     * @details Constructor que copia todo el contenido de un objeto del mismo tipo pasado por parámetro.
     * @param T Referencia constante que apunta al objeto del cual queremos hacer la copia de los datos.
     */
    Track(const Track& T);

    /**
     * @brief Destructor de la clase 'Track'.
     */
    ~Track();

    /**
     * @brief Método getter para obtener la posición actual dentro del camino.
     * @return std::pair<unsigned int, unsigned int>.( .first -> Componente x. / .second -> Componente y).
     */
    std::pair<unsigned int, unsigned int> getPos_actual_() const;

    /**
     * @brief Método getter para obtener el número de filas.
     * @return Coste del camino (float).
     */
    float getCost_() const;

    /**
     * @brief Método setter para modificar el coste del camino.
     * @details Método al que se le pasa por parámetros el nuevo coste del camino.
     * @param cost_ Nuevo coste del camino.
     */
    void setCost_(float cost_);

    /**
     * @brief Método getter para obtener el número de pasos del camino.
     * @return Número de pasos (int).
     */
    int getN_steps_() const;

    /**
     * @brief Método insertar por el final.
     * @details Método al que se le pasa por parámetros una nueva posición del camino que será insertada al final.
     * @param pos Nueva posición dentro del camino.
     */
    void append(std::pair<unsigned int, unsigned int> pos);

    /**
     * @brief Método insertar por el principio.
     * @details Método al que se le pasa por parámetros una nueva posición del camino que será insertada al principio.
     * @param pos Nueva posición dentro del camino.
     */
    void front_append(std::pair<unsigned int, unsigned int> pos);

    /**
     * @brief Método getter para obtener la última posicíon del camino.
     * @return Última posición del camino(std::pair<unsigned int, unsigned int>).
     */
    std::pair<unsigned int, unsigned int> get_last() const;

    /**
     * @brief Sobrecarga del operador <.
     */
    bool operator<(const Track& rhs) const;

    /**
     * @brief Sobrecarga del operador >.
     */
    bool operator>(const Track& rhs) const;

    /**
     * @brief Sobrecarga del operador <=.
     */
    bool operator<=(const Track& rhs) const;

    /**
     * @brief Sobrecarga del operador >=.
     */
    bool operator>=(const Track& rhs) const;

    /**
     * @brief Método para hallar la equivalencia con un camino.
     * @details Método que comprueba si el camino pasado por parámetro es 'equivalente', entendiendo por caminos equivalentes aquellos que tienen el mismo punto final.
     * @return Retorna true en caso de que el camino sea equivalente, false en caso contrario.
     */
    bool equivalent(const Track& rhs) const;

    /**
     * @brief Método para iniciar el recorrido sobre el camino.
     * @details Inicializa la variable 'pos_actual_' a la primera posición del camino para comenzar a recorrerlo.
     */
    void start_track();

    /**
     * @brief Método para obtener el siguiente paso a dar en el camino
     * @details Método que devuelve la siguiente posición del camino según el parámetro i. En caso de ser 0, primero se hace una llamada a start_track y después se devuelve la posición.
     * @param i Posición dentro del vector a la que queremos acceder del camino("número de paso que queremos obtener").
     * @return Siguiente posición a transitar para seguir recorriendo el camino.
     */
    std::pair<unsigned int, unsigned int> next_step(int i);

};
