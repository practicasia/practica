#include "Car.h"

Car::Car() :
        MatrixObject()
{}


Car::Car(unsigned int x_, unsigned int y_) :
        MatrixObject(x_, y_, '@')
{}

Car::Car(const MatrixObject& MO) :
        MatrixObject(MO)
{}

Car::~Car()
{}

bool Car::is_empty()
{
    return false;
}

void Car::show()
{
    std::cout << "@";
}

bool Car::is_end()
{
    return false;
}

bool Car::is_block()
{
    return false;
}
