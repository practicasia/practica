#pragma once
#include "MatrixObject.h"


/**
 * @class Empty
 * @brief Clase que representa el vacío dentro de la matriz(posiciones transitables).
 * @details Clase hija de 'MatrixObject' con lo que tiene sus mismos atributos.
 */
class Empty : public MatrixObject
{

public:

    /**
     * @brief Constructor por defecto de la clase 'Empty'.
     * @details Constructor que llama al constructor por defecto de la clase 'MatrixObject'.
     */
    Empty();

    /**
     * @brief Constructor que inicializa los valores de la clase 'Empty'.
     * @details Constructor que llama al constructor de la clase 'MatrixObject' con los parámetros pasados.
     * @param x_ Número de fila en la matriz.
     * @param y_ Número de columna en la matriz.
     */
    Empty(unsigned int x_, unsigned int y_);

    /**
     * @brief Constructor de copia de la clase 'Empty'
     * @details Constructor que copia todo el contenido de un objeto del mismo tipo pasado por parámetro.
     * @param MO Referencia constante que apunta al objeto del cual queremos hacer la copia de los datos.
     */
    Empty(const MatrixObject& MO);

    /**
     * @brief Destructor de la clase 'Empty'.
     */
    virtual ~Empty();

    /**
     * @brief Método que devuelve si la clase es de tipo 'Empty'
     * @return Retorna siempre true ya que es del tipo 'Empty'(vacío).
     */
    virtual bool is_empty() override;

    /**
     * @brief Método para mostrar por pantalla el contenido de la clase 'Empty'.
     */
    virtual void show() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'End'
     * @return Retorna siempre false ya que no representamos el punto de destino del coche.
     */
    virtual bool is_end() override;

    /**
     * @brief Método que devuelve si la clase es de tipo 'Block'
     * @return Retorna siempre false ya que el vacío representa lo contrario a 'Block'.
     */
    virtual bool is_block() override;
};
