#baifo@etsii.ull.es
#OBJS especifica que archivos tenemos que compilar
OBJS = Block.cpp Car.cpp Empty.cpp End.cpp Matrix.cpp MatrixObject.cpp Track.cpp node.cpp main.cpp

#CC especifica el compilador
CC = g++

#COMPILER_FLAGS especifica información adicional
COMPILER_FLAGS = -w -g -std=c++11

#LINKER_FLAGS especifica las librerias que vamos a linkar
LINKER_FLAGS = -lGL -lGLU -lglut

#OBJ_NAME especifica el nombre del ejecutable
OBJ_NAME = pract01

#comando
all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)
