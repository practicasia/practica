#include "Matrix.h"
#include "Track.h"

Matrix::Matrix()
{}

Matrix::Matrix(unsigned int m_, unsigned int n_) :
        m_(m_),
        n_(n_)
{

    V_.resize(Matrix::m_);

    for (unsigned int i=0;i<Matrix::m_;++i) {

        V_[i].resize(Matrix::n_);

        for (unsigned int j=0;j<Matrix::n_;++j) {

            MatrixObject* aux;

            aux = new Empty(i,j);

            V_[i][j] = aux;

        }
    }
}

Matrix::~Matrix()
{}

unsigned int Matrix::get_m(){
  return m_;
}

unsigned int Matrix::get_n(){
  return n_;
}

MatrixObject* Matrix::get_celda(int i, int j){
  return V_[i][j];
}

std::vector<int> Matrix::get_pos_car(){
  std::vector<int> dummy;
  dummy.push_back(pos_x_coche_);
  dummy.push_back(pos_y_coche_);
  return dummy;
}

void Matrix::set_pos_car(int x, int i){
  pos_x_coche_ = x;
  pos_y_coche_ = i;
}

void Matrix::random_fill_matrix(unsigned int percent)
{
    this->percent = percent;
    unsigned int n_block = ((m_ * n_) * percent) / 100;

    srand(time(nullptr));   // Inicializamos semilla para generacion aleatoria

    for (unsigned int i = 0; i < n_block; ++i) {

        unsigned int x = rand() % m_;
        unsigned int y = rand() % n_;

        if ( V_[x][y]->is_empty() && // Condiciones para que no rellene ni la primera ni ultima pos
                (x != 0) && (x != m_-1) &&
                (y != 0) && (y != n_-1)) {

            MatrixObject* b_aux;
            b_aux = new Block(x, y);

            V_[x][y] = b_aux;
        }

        else    i--;
    }
}

void Matrix::manual_fill_matrix()
{
    std::string cad;

    do {

        this->show();

        std::cout << "Introduzca una posicion con formato \"x,y\" donde x es el numero de filas e y el de columna."
                  << std::endl;
        std::cout << "\t- Tener en cuenta que la posicion es 0,0." << std::endl;
        std::cout << "\t- Para dejar de introducir obstaculos introduzca \"end\"" << std::endl;
        std::cout << ">> ";
        std::cin >> cad;

        if (cad != "end") {

            int x = -1;
            int y = -1;

            size_t pos = cad.find(",");

            std::string s_aux;

            // Comprobamos que encuentra ","
            if (pos != std::string::npos) {

                s_aux = cad.substr(0, pos);

                // Comprobamos que la cadena extraida es numerica
                if (isdigit(s_aux[0])) {

                    x = stoul(s_aux);

                    cad.erase(0, pos + 1);
                }

                s_aux = cad.substr(0);

                // Comprobamos que la cadena extraida es numerica y no esta vacia
                if (!cad.empty() && isdigit(cad[0]))  //

                    y = stoul(s_aux);

            }

            // Comprobamos que las posiciones introducidas no se salen de la matriz
            if (x < m_ && x >= 0 && y < n_ && y >= 0 && V_[x][y]->is_empty()) {

                MatrixObject* b_aux;
                b_aux = new Block(x, y);

                V_[x][y] = b_aux;
            }
        }

    } while (cad != "end");
}
bool Matrix::locate_car(std::string pos_car)
{
    bool located = false;

    int x = -1;
    int y = -1;

    size_t pos = pos_car.find(",");

    std::string s_aux;

    // Comprobamos que encuentra ","
    if (pos != std::string::npos) {

        s_aux = pos_car.substr(0, pos);

        // Comprobamos que la cadena extraida es numerica
        if (isdigit(s_aux[0])) {

            x = stoul(s_aux);

            pos_car.erase(0, pos + 1);
        }

        s_aux = pos_car.substr(0);

        // Comprobamos que la cadena extraida es numerica y no esta vacia
        if (!pos_car.empty() && isdigit(pos_car[0]))  //

            y = stoul(s_aux);

    }
    // Comprobamos que las posiciones introducidas no se salen de la matriz
    if (x < m_ && x >= 0 && y < n_ && y >= 0 && V_[x][y]->is_empty()) {

        located = true;
        MatrixObject* b_aux;
        b_aux = new Car(x, y);

        V_[x][y] = b_aux;

        pos_x_coche_ = x;
        pos_y_coche_ = y;
    }

    //this->show();

    return located;
}

bool Matrix::locate_endPoint(std::string pos_end)
{
    bool located = false;

    int x = -1;
    int y = -1;

    size_t pos = pos_end.find(",");

    std::string s_aux;

    // Comprobamos que encuentra ","
    if (pos != std::string::npos) {

        s_aux = pos_end.substr(0, pos);

        // Comprobamos que la cadena extraida es numerica
        if (isdigit(s_aux[0])) {

            x = stoul(s_aux);

            pos_end.erase(0, pos + 1);
        }

        s_aux = pos_end.substr(0);

        // Comprobamos que la cadena extraida es numerica y no esta vacia
        if (!pos_end.empty() && isdigit(pos_end[0]))  //

            y = stoul(s_aux);

    }
    // Comprobamos que las posiciones introducidas no se salen de la matriz
    if (x < m_ && x >= 0 && y < n_ && y >= 0 && V_[x][y]->is_empty()) {

        located = true;
        MatrixObject* b_aux;
        b_aux = new End(x, y);

        V_[x][y] = b_aux;

        pos_x_end_ = x;
        pos_y_end_ = y;
    }

    return located;
}

void Matrix::show()
{
    std::cout << "|";
    for (int k=0;k<n_;++k)

        std::cout << "-";

    std::cout << "|" << std::endl;

    for (unsigned int i=0;i<m_;++i) {

        std::cout << "|";

        for (unsigned int j=0;j<n_;++j)

            (*V_[i][j]).show();

        std::cout << "| " << i << std::endl;
    }

    std::cout << "|";
    for (int k=0;k<n_;++k)

        std::cout << "-";

    std::cout << "|" << std::endl;
}


Track Matrix::A_estrella()
{

    //time_t initTime = time(NULL);
    clock_t initTime = clock();

    Track solucion;
    std::vector<node*> list;
    node* n0;
    node* m0;
    bool found = false;
    bool resign = false;

    int dir = 4;
    int delta[4][2] = {{-1, 0},
                       {0,  -1},
                       {1,  0},
                       {0,  1}};
    int coste = 1;

    int nodos_abiertos_[m_][n_];
    int nodos_cerrados_[m_][n_];
    int map_direcciones[m_][n_];

    for (int x = 0; x < m_; x++) {
        for (int y = 0; y < n_; y++) {
            nodos_cerrados_[x][y] = 0;
            nodos_abiertos_[x][y] = -1;
            map_direcciones[x][y] = -1;
        }
    }

    nodos_cerrados_[pos_x_coche_][pos_y_coche_] = 1;

    int x = pos_x_coche_;
    int y = pos_y_coche_;
    int g = 0;

    n0 = new node(g, x, y);

    list.push_back(n0);

    while (!found && !resign) {

        if (list.empty()) {
            resign = true;
            return solucion;

        } else {

            std::stable_sort(list.begin(), list.end());
            m0 = list.front();
            x = m0->getx();
            y = m0->gety();
            g = m0->getPrioridad();
            list.erase(list.begin());

            if (x == pos_x_end_ && y == pos_y_end_) {
                found = true;
            } else {

                for (int i = 0; i < 4; i++) {

                    int x2 = x + delta[i][0];
                    int y2 = y + delta[i][1];

                    if (x2 >= 0 && x2 < m_ && y2 >= 0 && y2 < n_) {

                        if (nodos_cerrados_[x2][y2] == 0 && V_[x2][y2]->is_empty()) {

                            int g2 = g + coste;
                            n0 = new node(g2, x2, y2);
                            list.push_back(n0);

                            //std::cout << std::endl;
                            nodos_cerrados_[x2][y2] = 1;
                            map_direcciones[x2][y2] = i;
                        }
                    }
                }
            }
        }
    }

    x = pos_x_end_;
    y = pos_y_end_;

    while (x != pos_x_coche_ || y != pos_y_coche_) {
        //std::cout << map_direcciones[x][y] << std::endl;
        int dire[2];
        dire[0] = x - delta[map_direcciones[x][y]][0];
        dire[1] = y - delta[map_direcciones[x][y]][1];

        solucion.front_append(std::pair<unsigned int, unsigned int>(dire[0], dire[1]));

        x = dire[0];
        y = dire[1];

    }

    //time_t endTime = time(NULL);
    //double duration = difftime(endTime, initTime);
    clock_t duration = clock() - initTime;  // Exportar el tiempo

    // Exportamos info importante a la estructura
    data_t data;
    data.row = this->get_m();
    data.col = this->get_n();
    data.percent = this->percent;
    data.steps = solucion.getN_steps_();
    data.time = ((double)duration)/CLOCKS_PER_SEC;

    std::cout << "\tTiempo: " << ((double)duration)/CLOCKS_PER_SEC << " s" << std::endl;

    data.exportData("./datos/data.json"); // Escribimos la info en el fichero

    return solucion;
}
